import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ChatItem, InputChat, Header} from '../../components/molecules';
import {colors, fonts} from '../../utils';

export default function index({navigation}) {
  return (
    <View style={styles.page}>
      <Header
        type="dark-profile"
        title="dr. Farida Aminingrum"
        onPress={() => navigation.goBack()}
      />
      <View style={styles.content}>
        <Text style={styles.chatDate}>Senin, 20 Desember, 2020</Text>
        <ChatItem isMe />
        <ChatItem />
        <ChatItem isMe />
      </View>
      <InputChat />
    </View>
  );
}

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  content: {flex: 1},
  chatDate: {
    fontSize: 11,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    marginVertical: 20,
    textAlign: 'center',
  },
});
