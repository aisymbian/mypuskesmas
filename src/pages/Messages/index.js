import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {DummyDoctor4, DummyDoctor5, DummyDoctor6} from '../../assets';
import {List} from '../../components';
import {colors, fonts} from '../../utils';

export default function index({navigation}) {
  const [doctors] = useState([
    {
      id: 1,
      profile: DummyDoctor4,
      name: 'Alexander Jennier',
      desc: 'Baik bu akwkaowokaowkoa',
    },
    {
      id: 2,
      profile: DummyDoctor5,
      name: 'Alexander Jennier',
      desc: 'Baik bu akwkaowokaowkoa',
    },
    {
      id: 3,
      profile: DummyDoctor6,
      name: 'awkar Jennier',
      desc: 'Baik bu akwkaowasssokaowkoa',
    },
  ]);
  return (
    <View style={styles.page}>
      <View style={styles.content}>
        <Text>Messages</Text>
        {doctors.map((doctor) => {
          return (
            <List
              key={doctor.id}
              profile={doctor.profile}
              name={doctor.name}
              desc={doctor.desc}
              onPress={() => navigation.navigate('Chatting')}
            />
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {backgroundColor: colors.secondary, flex: 1},
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 30,
    marginLeft: 16,
  },
});
