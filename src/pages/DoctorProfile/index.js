import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Header, Profile, ProfileItem, Gap} from '../../components';
import {colors} from '../../utils';

export default function index({navigation}) {
  return (
    <View style={styles.page}>
      <Header title="Doctor Profile" onPress={() => navigation.goBack()} />
      <Profile name="Nair" desc="dokter anak " />
      <Gap height={10} />
      <ProfileItem label="alumus" value="Universtas Indonesia, 2019" />
      <ProfileItem
        label="Tempat Praktik"
        value="Puskesmas Tambakaji Indonesia, 2019"
      />
      <ProfileItem label="No. STR" value="000000020202" />
      <View style={styles.action}>
        <Button title="Start Consultation" onPress={() => navigation.navigate('Chatting')}/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  action: {paddingHorizontal: 40, paddingTop: 23},
});
