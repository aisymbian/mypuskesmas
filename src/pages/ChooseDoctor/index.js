import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {DummyDoctor1} from '../../assets';
import {Header, List} from '../../components';
import {colors} from '../../utils';

export default function index({navigation}) {
  return (
    <View>
      <Header
        type="dark"
        title="Pilih Dokter Anak"
        onPress={() => navigation.goBack()}
      />
      <List
        type="next"
        profile={DummyDoctor1}
        name="Alendaner"
        desc="wanita"
        onPress={() => navigation.navigate('Chatting')}
      />
      <List type="next" profile={DummyDoctor1} name="Alendaner" desc="wanita" />
      <List type="next" profile={DummyDoctor1} name="Alendaner" desc="wanita" />
      <List type="next" profile={DummyDoctor1} name="Alendaner" desc="wanita" />
      <List type="next" profile={DummyDoctor1} name="Alendaner" desc="wanita" />
    </View>
  );
}

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
});
